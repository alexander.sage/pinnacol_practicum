#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 26 13:40:23 2021

@author: sage
"""

import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pandas as pd
from tensorflow.keras.layers import Embedding, LSTM, Dense, Bidirectional
import matplotlib.pyplot as plt
import random

def plot_graphs(history, string):
	plt.plot(history.history[string])
	plt.plot(history.history['val_'+string])
	plt.xlabel("Epochs")
	plt.ylabel(string)
	plt.legend([string, 'val_'+string])
	plt.show()
  
  
def clean_categories(categories) :
	'''not an ideal way to handle this. tensorflow wants to split Allergy / Immunology  
	into two tokens, but I want it to stay as one category.
	I wanted each string to be treated a single category. More work could 
	clean this up a little since now the category is just allergyimmunology'''
	categories = [x.replace(' ', '') for x in categories]
	categories = [x.replace('/', '') for x in categories]
	categories = [x.replace('.', '') for x in categories]
	categories = [x.replace('-', '') for x in categories]	
	return categories


vocab_size = 10000
embedding_dim = 16
max_length = 128
trunc_type='post'
oov_tok = "<OOV>"

##loading
train_df = pd.read_csv('train_data.csv')
train_df = train_df.dropna()
records = train_df.shape[0]
categories = train_df['medical_specialty'].to_numpy()
categories = clean_categories(categories)
sentences = train_df['transcription'].to_numpy()
sentences = [x.split() for x in sentences]
ids = [i for i in range(len(sentences))]
temp = list(zip(ids, sentences, categories))
random.shuffle(temp)
ids, sentences, categories = zip(*temp)
ct_categories = len(set(categories))
print("categories", ct_categories)

##splitting
split = int(records*0.7)
train_x = sentences[:split]
train_y = categories[:split]
test_x = sentences[split:]
test_y = categories[split:]

##tokenizing
tokenizer_x = Tokenizer(num_words = vocab_size, oov_token=oov_tok)
tokenizer_x.fit_on_texts(train_x)
word_index_x = tokenizer_x.word_index
sequences = tokenizer_x.texts_to_sequences(train_x)
train_padded = pad_sequences(sequences,maxlen=max_length, truncating=trunc_type, padding='pre')
print("shape train", train_padded.shape)
input_words = len(word_index_x) + 1

tokenizer_y = Tokenizer(num_words = ct_categories, oov_token=oov_tok)
tokenizer_y.fit_on_texts(categories)
word_index_y = tokenizer_y.word_index
labels = tokenizer_y.texts_to_sequences(train_y)
print("shape labels", len(labels), labels[0])
train_categ = tf.keras.utils.to_categorical(labels, num_classes=ct_categories)
print("training categorical", train_categ.shape)

testing_sequences = tokenizer_x.texts_to_sequences(test_x)
testing_padded = pad_sequences(testing_sequences,maxlen=max_length)
test_labels = tokenizer_y.texts_to_sequences(test_y)
test_categ = tf.keras.utils.to_categorical(test_labels, num_classes=ct_categories)

###training
model = tf.keras.models.Sequential()
model.add(Embedding(input_words, 64, input_length=max_length))
model.add(Bidirectional(LSTM(20)))
model.add(Dense(32, activation='relu'))
model.add(Dense(ct_categories, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
history = model.fit(train_padded, train_categ, epochs=20, verbose=1, validation_data=(testing_padded, test_categ))

plot_graphs(history, 'accuracy')
plt.savefig('training_accuracy.png')