#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 26 10:32:13 2021

@author: sage
for Pinnacol Assurance Practicum
"""

import pandas as pd
import numpy as np
import regex as re
import csv
import matplotlib.pyplot as plt

##globals
DESCRIPTION = 'description'
SPECIALTY = 'medical_specialty'
OPERATION = 'sample_name'
KEYWORDS = 'keywords'
TRANSCRIPTION = 'transcription'

def split_keywords(df):
	'''takes in the dataframe and modifies it in place to go from string to 
	list of strings'''
	uniq_keywords = set()
	for ind, row in df.iterrows():
		##splits and then removes empty values	
		line = row[KEYWORDS]
		line = re.sub("[^A-Za-z', ]+", '',line)
# 		line = line.replace('note', ' ')
		
		keyword_list = list(filter(None, line.split(',')))
		keyword_list  = [wrd.strip() for wrd in keyword_list]
		df.at[ind, KEYWORDS]	=  keyword_list 
		[uniq_keywords.add(wrd) for wrd in keyword_list]	
	return uniq_keywords	
	
def clean_transcript(df):
	'''cleans the transcript column so it can be ready for NLP'''
	for ind, row in df.iterrows():
		clean = row[TRANSCRIPTION]
		clean = re.sub("[^A-Za-z' ]+", ' ',clean)
# 		if clean=='':
# 			print(row)
		df.at[ind, TRANSCRIPTION]	= clean 

def load_csv():
	'''loads the csv file, cleans some fields a little,
	returns a data frame'''

	fname = 'mtsamples.csv'
	df = pd.read_csv(fname, dtype={'index':int, DESCRIPTION:str, SPECIALTY:str, OPERATION:str, TRANSCRIPTION:str, KEYWORDS:str})
	df = df.replace(np.nan, '', regex=True)
	df[DESCRIPTION] = df[DESCRIPTION].str.lower()
	df[SPECIALTY] = df[SPECIALTY].str.lower()
	df[OPERATION] = df[OPERATION].str.lower()
	df[TRANSCRIPTION] = df[TRANSCRIPTION].str.lower()
	df[KEYWORDS] = df[KEYWORDS].str.lower()
	return df

def save_to_csv(name, values):
	'''saves single list of values to csv'''
	with open (name+'.csv', 'w') as fileobj:
		csvwriter = csv.writer(fileobj, dialect='excel')
		csvwriter.writerow(values)

if __name__=='__main__':

	df = load_csv()
	uniq_specialty = set(df[SPECIALTY])
	
	print("\ndescription")
	empty_description= (df[DESCRIPTION].values=='').sum()
	print("empty values", empty_description)
	
	print("\nspecialties")
	print("unique", len(uniq_specialty))
	empty_specialty = (df[SPECIALTY].values=='').sum()
	print("empty values", empty_specialty)
	save_to_csv('unique_specialties', uniq_specialty)
	names_specialties = list(list(uniq_specialty))
	occurances = []
	for spec in names_specialties:
		occurances.append((df[SPECIALTY].values==spec).sum())
	##too many values to plot on pie chart
# 	fig, ax = plt.subplots()
# 	ax.pie( occurances, labels=names_specialties, autopct='%1.1f%%')
# 	plt.savefig('specialties.png')
	sp_df = pd.DataFrame({'specialties':names_specialties, 'counts': occurances})
	sp_df.to_csv('specialty_counts.csv')

	print("\noperations")
	uniq_operations = set(df[OPERATION])
# 	uniq_operations.remove('')
	print("unique ", len(uniq_operations))
	empty_operations = (df[OPERATION].values=='').sum()
	print("empty values", empty_operations)
# 	print("percent empty", round(empty_operations/df.shape[0]*100), "%")
	save_to_csv('unique_sample_name_operations', uniq_operations)

	print("\ntranscription")
	empty_transcription = (df[TRANSCRIPTION].values=='').sum()
	print("empty values", empty_transcription)
	print("percent empty", round(empty_transcription/df.shape[0]*100), "%")
		
	empty_keywords = (df[KEYWORDS].values=='').sum()	
	uniq_keywords  = split_keywords(df)
	uniq_keywords.remove('')
	print("\nkeywords")
	print("unique ", len(uniq_keywords))
	print('empty values', empty_keywords)
	print("percent empty", round(empty_keywords/df.shape[0]*100), "%")
	save_to_csv('unique_keywords', list(uniq_keywords))
	
	clean_transcript(df)
	train_df = df.drop(columns=['index',DESCRIPTION, OPERATION, KEYWORDS])
	trian_df = train_df[train_df[TRANSCRIPTION].values!='']
	train_df = train_df.dropna()
	train_df.to_csv('train_data.csv')
	
	
	